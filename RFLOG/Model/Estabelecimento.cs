﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using RFLOG.Util;

namespace RFLOG.Model
{
    /// <summary>
    ///     Modelo de Estabelecimento.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    class Estabelecimento
    {

        public long id { get; set; }

        public string descricao { get; set; }

        public bool validade { get; set; }

        public string cnpj { get; set; }

        public List<Setor> setores { get; set; }

        public bool registroAtivo { get; set; }

        public Estabelecimento()
        {
        }

        public Estabelecimento(XmlNode xmlNode)
        {
            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if ("id".ToLower().Equals(node.Name.ToLower()))
                {
                    this.id = long.Parse(node.InnerText);
                }
                if ("descricao".ToLower().Equals(node.Name.ToLower()))
                {
                    this.descricao = node.InnerText;
                }
                if ("cnpj".ToLower().Equals(node.Name.ToLower()))
                {
                    this.cnpj = node.InnerText;
                }
                if ("validade".ToLower().Equals(node.Name.ToLower()))
                {
                    this.validade = bool.Parse(node.InnerText);
                }
                if ("registroAtivo".ToLower().Equals(node.Name.ToLower()))
                {
                    this.registroAtivo = bool.Parse(node.InnerText);
                }
                if ("setores".ToLower().Equals(node.Name.ToLower()))
                {
                    if (setores == null)
                    {
                        setores = new List<Setor>();
                    }
                    foreach (XmlNode setorNode in node.ChildNodes)
                    {
                        this.setores.Add(new Setor(setorNode));
                    }
                }
            }
        }

        public XmlDocument getXML()
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(this.ToString());
            return xml;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(this.GetType().Name.ToLower()).Append(">");
            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                if ("setores".Equals(property.Name.ToLower()))
                {
                    sb.Append("<").Append(property.Name).Append(">");
                    foreach (var setor in setores)
                    {
                        sb.Append(setor.ToString());
                    }
                    sb.Append("</").Append(property.Name).Append(">");
                }
                else
                {
                    sb.Append("<").Append(property.Name).Append(">");
                    sb.Append(property.GetValue(this, null));
                    sb.Append("</").Append(property.Name).Append(">");
                }
            }
            sb.Append("</").Append(this.GetType().Name.ToLower()).Append(">");
            return sb.ToString();
        }
    }
}
