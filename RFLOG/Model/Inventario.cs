﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using RFLOG.Util;

namespace RFLOG.Model
{
    class Inventario
    {
        public long id { get; set; }

        public DateTime dataInventario { get; set; }

        public Setor setor { get; set; }

        public List<Tag> tags { get; set; }

        public bool registroAtivo { get; set; }

        public Inventario()
        {
        }

        public Inventario(XmlNode xmlNode)
        {
            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if ("id".Equals(node.Name))
                {
                    this.id = long.Parse(node.InnerText);
                }
                if ("dataInventario".Equals(node.Name))
                {
                    this.dataInventario = DateTime.Parse(node.InnerText);
                }
                if ("registroAtivo".Equals(node.Name))
                {
                    this.registroAtivo = bool.Parse(node.InnerText);
                }
                if ("setor".Equals(node.Name))
                {
                    this.setor = new Setor(node);
                }
                if ("tags".Equals(node.Name))
                {
                    if (tags == null)
                    {
                        tags = new List<Tag>();
                    }
                    foreach (XmlNode tagNode in node.ChildNodes)
                    {
                        this.tags.Add(new Tag(tagNode));
                    }
                }
            }
        }

        public XmlDocument getXML()
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(this.ToString());
            return xml;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(this.GetType().Name.ToLower()).Append(">");
            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                if ("tags".Equals(property.Name.ToLower()))
                {
                    sb.Append("<").Append(property.Name.ToLower()).Append(">");
                    foreach (var tag in tags)
                    {
                        sb.Append(tag.ToString());
                    }
                    sb.Append("</").Append(property.Name.ToLower()).Append(">");
                }
                else if ("setor".Equals(property.Name.ToLower()))
                {
                    sb.Append(setor.ToString());
                }
                else
                {
                    sb.Append("<").Append(property.Name.ToLower()).Append(">");
                    sb.Append(property.GetValue(this, null));
                    sb.Append("</").Append(property.Name.ToLower()).Append(">");
                }
            }
            sb.Append("</").Append(this.GetType().Name.ToLower()).Append(">");
            return sb.ToString();
        }

    }
}
