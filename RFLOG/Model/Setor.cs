﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using RFLOG.Util;

namespace RFLOG.Model
{
    /// <summary>
    ///     Modelo de Setor.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    class Setor
    {
        public long id { get; set; }

        public string descricao { get; set; }

        public bool registroAtivo { get; set; }

        public Setor()
        {
        }

        public XmlDocument getXML()
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(this.ToString());
            return xml;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(this.GetType().Name.ToLower()).Append(">");
            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                sb.Append("<").Append(property.Name).Append(">");
                sb.Append(property.GetValue(this, null));
                sb.Append("</").Append(property.Name).Append(">");
            }
            sb.Append("</").Append(this.GetType().Name.ToLower()).Append(">");
            return sb.ToString();
        }

        public Setor(XmlNode xmlNode)
        {
            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if ("id".ToLower().Equals(node.Name.ToLower()))
                {
                    this.id = long.Parse(node.InnerText);
                }
                if ("descricao".ToLower().Equals(node.Name.ToLower()))
                {
                    this.descricao = node.InnerText;
                }
                if ("registroAtivo".ToLower().Equals(node.Name.ToLower()))
                {
                    this.registroAtivo = bool.Parse(node.InnerText);
                }
            }
        }
    }
}
