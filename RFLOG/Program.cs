﻿using System;

using System.Collections.Generic;
using System.Windows.Forms;

namespace RFLOG
{
    static class Program
    {
        /// <summary>
        /// Entrada Principal Para A Aplicação.
        /// </summary>
        
        [MTAThread]
        static void Main()
        {
            Application.Run(new Tela.RFLOG());
        }
    }
}