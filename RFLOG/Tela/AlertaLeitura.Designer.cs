﻿namespace RFLOG.Tela
{
    partial class AlertaLeitura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer();
            this.tempo = new System.Windows.Forms.Label();
            this.labelSegundos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.Font = new System.Drawing.Font("Courier New", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label.Location = new System.Drawing.Point(29, 56);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(181, 57);
            this.label.Text = "Lendo";
            this.label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // tempo
            // 
            this.tempo.Font = new System.Drawing.Font("Tahoma", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.tempo.ForeColor = System.Drawing.Color.Red;
            this.tempo.Location = new System.Drawing.Point(29, 113);
            this.tempo.Name = "tempo";
            this.tempo.Size = new System.Drawing.Size(181, 22);
            this.tempo.Text = "0";
            this.tempo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelSegundos
            // 
            this.labelSegundos.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold);
            this.labelSegundos.ForeColor = System.Drawing.Color.OliveDrab;
            this.labelSegundos.Location = new System.Drawing.Point(29, 135);
            this.labelSegundos.Name = "labelSegundos";
            this.labelSegundos.Size = new System.Drawing.Size(181, 20);
            this.labelSegundos.Text = "Segundos";
            this.labelSegundos.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // AlertaLeitura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(237, 231);
            this.ControlBox = false;
            this.Controls.Add(this.labelSegundos);
            this.Controls.Add(this.tempo);
            this.Controls.Add(this.label);
            this.MinimizeBox = false;
            this.Name = "AlertaLeitura";
            this.Text = "Mensagem";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label tempo;
        private System.Windows.Forms.Label labelSegundos;
    }
}