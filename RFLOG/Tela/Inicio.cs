﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela inicial das atividades do sistema.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    public partial class Inicio : Form
    {

        private RFLOG rflog;

        public Inicio()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }

        private void inventario_Click(object sender, EventArgs e)
        {
            RFLOG.Selecionar.Show();
        }

        public RFLOG RFLOG
        {
            get
            {
                if (rflog == null)
                {
                    rflog = new RFLOG();
                }
                return rflog;
            }

            set
            {
                rflog = value;
            }
        }

        public void clear()
        {

        }

        private void fechar_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void atualizar_Click(object sender, EventArgs e)
        {
            RFLOG.getEstabelecimentoList();
            RFLOG.Selecionar.lerXmlEstabelecimento();
        }
    }
}