﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela faz as leituras das Tags.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    public partial class Leitura : Form
    {

        public Leitura()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }

        private void back_Click(object sender, EventArgs e)
        {
            RFLOG.Selecionar.Show();
        }

        private void next_Click(object sender, EventArgs e)
        {
            RFLOG.Resultado.Show();
        }

        public void clear()
        {  
            
        }

        private RFLOG rflog;

        public RFLOG RFLOG
        {
            get
            {
                if (rflog == null)
                {
                    rflog = new RFLOG();
                }
                return rflog;
            }

            set
            {
                rflog = value;
            }
        }

        public ListBox ListaTags
        {
            get
            {
                return listBox;
            }

            set
            {
                listBox = value;
            }
        }

        public Label Quantidade
        {
            get
            {
                return quantidade;
            }

            set
            {
                quantidade = value;
            }
        } 
    }
}