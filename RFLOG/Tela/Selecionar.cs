﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using RFLOG.Model;
using RFLOG.Util;
using System.Xml;
using System.IO;

namespace RFLOG.Tela
{
    /// <summary>
    ///     Tela de Seleção de Estabelecimento e Setor.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    public partial class Selecionar : Form
    {

        private RFLOG rflog;

        private List<Estabelecimento> collection = new List<Estabelecimento>();

        public Selecionar()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            lerXmlEstabelecimento();
        }

        public void lerXmlEstabelecimento()
        {
            if (!FileUtil.IsNotExistFile(Mensagem.NOME_ARQUIVO_ESTABELECIMENTO))
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(Mensagem.NOME_ARQUIVO_ESTABELECIMENTO);
                collection.Clear();
                foreach (XmlNode node in xmlDocument.LastChild.ChildNodes)
                {
                    Estabelecimento estabelecimento = new Estabelecimento(node);
                    collection.Add(estabelecimento);
                    this.estabelecimento.Items.Add(estabelecimento.descricao);
                    System.Console.Out.WriteLine(estabelecimento);
                }
            }
        }

        private void next_Click(object sender, EventArgs e)
        {
            RFLOG.Leitura.Show();
        }

        private void back_Click(object sender, EventArgs e)
        {
            RFLOG.Inicio.Show();
        }

        public RFLOG RFLOG
        {
            get
            {
                if (rflog == null)
                {
                    rflog = new RFLOG();
                }
                return rflog;
            }

            set
            {
                rflog = value;
            }
        }

        public void clear()
        {
        
        }

        private void estabelecimento_SelectedIndexChanged(object sender, EventArgs e)
        {
            setor.Items.Clear();
            foreach (Estabelecimento estabelecimentoLocal in collection)
            {
                if (estabelecimentoLocal.descricao.Equals(this.estabelecimento.SelectedItem))
                {
                    foreach (Setor setorLocal in estabelecimentoLocal.setores)
                    {
                        this.setor.Items.Add(setorLocal.descricao);
                    }
                    break;
                }
            }
        }
    }
}