﻿using System;

using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace RFLOG.Util
{
    /// <summary>
    ///     Utilitário para tratamento de arquivos.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    
    class FileUtil
    {
        public static bool IsNotExistFile(string fileName)
        {
            return !File.Exists(fileName);
        }

        public static bool SaveXML(XmlDocument xmlDocument)
        {
            bool retorno = true;
            if (xmlDocument != null)
            {
                FileUtil.ReCreateFile(Mensagem.NOME_ARQUIVO_ESTABELECIMENTO);
                xmlDocument.Save(new FileStream(Mensagem.NOME_ARQUIVO_ESTABELECIMENTO, FileMode.Open, FileAccess.ReadWrite, FileShare.Read));
            }
            else
            {
                retorno = false;
            }
            return retorno;
        }

        public static void CreateFile(string fileName)
        {
            File.Create(fileName).Close();
        }

        public static void DeleteFile(string fileName)
        {
            try
            {
                File.Delete(fileName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void ReCreateFile(string fileName)
        {
            if (IsNotExistFile(fileName))
            {
                CreateFile(fileName);
            }
            else
            {
                DeleteFile(fileName);
                CreateFile(fileName);
            }
        }

        public static void WriteIntoFile(string fileName, string content)
        {
            StreamWriter file = null;
            try
            {
                file = new StreamWriter(new FileStream(fileName, FileMode.Truncate), Encoding.ASCII);
                file.Write(content);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Mensagem.ST_ERRO_UP, MessageBoxButtons.OK,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button1);
            }
            finally
            {
                if (file != null)
                {
                    file.Close();
                }
            }
        }
    }
}
