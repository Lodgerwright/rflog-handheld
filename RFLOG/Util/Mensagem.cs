﻿using System;

using System.Collections.Generic;
using System.Text;

namespace RFLOG.Util
{
    class Mensagem
    {
        public static string NOME_ARQUIVO_ESTABELECIMENTO = @"Estabelecimentos.XML";

        public static string LISTAR_ESTABELECIMENTO_WEB_SERVICE = @"http://45.55.144.49/service/estabelecimento/XML/listar";

        public static string XML_SUCESSO = @"XML Atualizado Com Sucesso";

        public static string XML_FALHA = @"Falha ao atualizar o XML";

        public static string ST_ERRO_UP = @"ERRO";

        public static string ST_SUCESSO_UP = @"SUCESSO";
    }
}
