﻿using System;

using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;


namespace RFLOG.Util
{

    /// <summary>
    ///     Classe de Cliente RESTful.
    ///     @author Raul Silveira Bento (bentoraul@gmail.com)
    /// </summary>
    class RestClient
    {
        public static System.Xml.XmlDocument MakeRequest(string requestUrl, string method, string content)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                if (content != null)
                {
                    byte[] bytes;
                    bytes = System.Text.Encoding.ASCII.GetBytes(content);
                    request.ContentType = "text/xml; encoding='utf-8'";
                    request.ContentLength = bytes.Length;
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                }
                request.Method = method;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(response.GetResponseStream());
                return (xmlDoc);

            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message, Mensagem.ST_ERRO_UP, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return null;
            }
        }
    }
}
